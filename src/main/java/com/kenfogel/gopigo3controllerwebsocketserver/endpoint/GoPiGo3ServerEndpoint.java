/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kenfogel.gopigo3controllerwebsocketserver.endpoint;

import com.kenfogel.gopigo3controllerwebsocketserver.control.GoPiGoBacking;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author omni_
 */
@ServerEndpoint("/server")
public class GoPiGo3ServerEndpoint {

    @Inject
    private GoPiGoBacking gopigo;

    static final Logger LOG = Logger.getLogger(GoPiGo3ServerEndpoint.class.getName());

    private static final String DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";

    @OnMessage
    public String onMessage(String message, Session session) throws IOException, InterruptedException {
        try {
            LOG.log(Level.INFO, "Message from client: {0}", message);

            switch (message) {
                case "Forward":
                    gopigo.goForward();
                    break;
                case "Backwards":
                    gopigo.goReverse();
                    break;
                case "Stop":
                    gopigo.goStop();
                    break;
                case "EysesOn":
                    gopigo.goTurnOffBothLED();
                    break;
                case "EyesOff":
                    gopigo.goTurnOnBothLED();
                    break;
                case "SensorOn":
                    gopigo.startUltra();
                    break;
                case "SensorOff":
                    gopigo.stopUltra();
                    break;
                case "ServoOn":
                    gopigo.startServo();
                    break;
                case "ServoOff":
                    gopigo.stopServo();
                    break;
                default:
                    LOG.log(Level.INFO, "Bad message from client: {}", message);
            }

        } catch (IOException | InterruptedException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        String serverMessage = new StringBuilder(100).append("Message processed by GoPiGo3ServerEndpoint at [").append(new SimpleDateFormat(DATE_FORMAT).format(new Date())).append("]").append(message)
                .toString();
        session.getBasicRemote().sendText("Eat Mooses");
        return serverMessage;
    }

}
